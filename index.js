const express = require("express");
const app = express();
const bodyParser = require("body-parser");

// Middleware
app.use(bodyParser.json());

// Store events in memory (replace with a database in production)
let events = [];

// Endpoint: Get event by ID
app.get("/api/v3/app/events", (req, res) => {
  const eventId = req.query.id;
  const event = events.find((event) => event.uid === eventId);

  if (event) {
    res.json(event);
  } else {
    res.status(404).json({ error: "Event not found" });
  }
});

// Endpoint: Get latest events
app.get("/api/v3/app/events", (req, res) => {
  const limit = parseInt(req.query.limit) || 5;
  const page = parseInt(req.query.page) || 1;
  const startIndex = (page - 1) * limit;
  const endIndex = startIndex + limit;
  const latestEvents = events.slice(startIndex, endIndex);

  res.json(latestEvents);
});

// Endpoint: Create a new event
app.post("/api/v3/app/events", (req, res) => {
  const event = req.body;
  events.push(event);
  res.json({ message: "Event created successfully" });
});

// Endpoint: Get event by ID
app.get("/api/v3/app/events/:id", (req, res) => {
  const eventId = req.params.id;
  const event = events.find((event) => event.uid === eventId);

  if (event) {
    res.json(event);
  } else {
    res.status(404).json({ error: "Event not found" });
  }
});

// Endpoint: Update event by ID
app.put("/api/v3/app/events/:id", (req, res) => {
  const eventId = req.params.id;
  const eventIndex = events.findIndex((event) => event.uid === eventId);

  if (eventIndex !== -1) {
    const updatedEvent = { ...events[eventIndex], ...req.body };
    events[eventIndex] = updatedEvent;
    res.json({ message: "Event updated successfully" });
  } else {
    res.status(404).json({ error: "Event not found" });
  }
});

// Start the server
const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
